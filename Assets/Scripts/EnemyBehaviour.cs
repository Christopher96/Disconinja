﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {

	private Animator anim;

	void Start(){
		anim = GetComponent<Animator>();
	}

	void OnCollisionEnter2D( Collision2D col ){
		if( col.gameObject.CompareTag( "Player" ) ){
			if( Player.isSlicing ){
				anim.SetTrigger( "Dead-slice" );
			} else {
				anim.SetTrigger( "Slice" );
			}
		}
	}

	void OnTriggerEnter2D( Collider2D col ){
		if( col.name.Contains( "Shuriken" ) ){
			anim.SetTrigger( "Dead-shuriken");
		}
	}
}
