﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class GUIUpdate : MonoBehaviour {
	
	float paddingTop, paddingLeft, areaWidth, areaHeight;
	
	void Start(){
		setValues();
		checkPlayerPrefs();
	}
	
	private Vector2 scrollPosition;

	void Update(){
		
		foreach( Touch touch in Input.touches ){
			if (touch.phase == TouchPhase.Moved){
				// DRAGGING SCROLLBAR
				scrollPosition.y += touch.deltaPosition.y * 2;
			}
		}

	}

	void setValues(){
		paddingTop = Screen.height / 8;
		paddingLeft = Screen.width / 14;
		
		areaWidth = Screen.width - paddingLeft * 2;
		areaHeight = Screen.height - paddingTop * 2;
		
		currentPath = Directory.GetCurrentDirectory();
		newPath = currentPath;
		
		_GUIState = GUIStates.mainMenu;

	}

	private bool loadingSelected = false;

	void checkPlayerPrefs(){
		string filePath = PlayerPrefs.GetString( "selectedClipPath" );
		if ( filePath != "" && filePath != null ){
			loadingSelected = true;
			StartCoroutine( waitForInit( filePath ) );
		}
	}

	IEnumerator waitForInit( string filePath ) {
		while( Sounds.loadedClips == null ) yield return null;
		loadNewClip( filePath, true );
	}

	public GUIStyles _GUIStyles;
	public GUIImages _GUIImages;

	private string currentPath, newPath;
	
	private enum GUIStates { mainMenu, soundSelect, pauseMenu, instructions };
	private GUIStates _GUIState;

	void OnGUI(){
		if ( GameUpdate.isPlaying ) return;
		if ( Player.isDead ) _GUIState = GUIStates.pauseMenu;

		GUI.contentColor = Color.cyan;
		GUI.backgroundColor = Color.black;

		GUI.skin.box.fixedWidth = areaWidth - 25;

		GUILayout.BeginArea( new Rect( paddingLeft, paddingTop, areaWidth, areaHeight ) );
		{
			switch( _GUIState ){
				case GUIStates.mainMenu: mainMenu(); break;
				case GUIStates.pauseMenu: pauseMenu(); break;
				case GUIStates.instructions: instructions(); break;
				case GUIStates.soundSelect: soundSelect(); break;
			}
		}
		GUILayout.EndArea();
	}

	void mainMenu(){

		scrollPosition = GUILayout.BeginScrollView( scrollPosition );
		{
			GUILayout.Label( "Disco ninja", _GUIStyles.titleLabel );

			if( GUILayout.Button( "Play", _GUIStyles.navButton ) ){
				GameUpdate.isPlaying = true;
			}

			if( GUILayout.Button( "Instructions", _GUIStyles.navButton ) ){
				_GUIState = GUIStates.instructions;
			}

			GUILayout.Label( "Current sound track", _GUIStyles.titleLabel );

			if( Sounds.selectedClip != null ){
				GUILayout.BeginHorizontal();
				{
					GUILayout.Label( Sounds.selectedClip.fileName, _GUIStyles.textLabel );
					if( GUILayout.Button( _GUIImages.remove, _GUIStyles.backButton ) ){
						Sounds.selectedClip = null;
						PlayerPrefs.SetString( "selectedClipPath", null );
					}
				}
				GUILayout.EndHorizontal();
			} else {
				string text = "Default sound track";
				if ( loadingSelected ) text = "Loading selected sound track...";
				GUILayout.Label( text, _GUIStyles.textLabel );
			}
			
			if( GUILayout.Button( "Select a new sound track", _GUIStyles.navButton ) ){
				_GUIState = GUIStates.soundSelect;
			}
		}
		GUILayout.EndScrollView();

	}

	void pauseMenu(){
		scrollPosition = GUILayout.BeginScrollView( scrollPosition );
		{
			if ( GUILayout.Button( "Main Menu", _GUIStyles.navButton ) ){
				GameUpdate.ReloadGame();
			}

			if ( GUILayout.Button( "Restart", _GUIStyles.navButton ) ){
				GameUpdate.isPlaying = true;
				GameUpdate.ReloadGame();
			}

			GUILayout.Label ("Distance", _GUIStyles.titleLabel);
			GUILayout.Label (Mathf.Floor (GameUpdate.currentScore).ToString () + " m", _GUIStyles.textLabel);

			GUILayout.Label ("Farthest distance", _GUIStyles.titleLabel);
			GUILayout.Label (Mathf.Floor (GameUpdate.highScore).ToString () + " m", _GUIStyles.textLabel);
		}
		GUILayout.EndScrollView ();
	}

	void instructions(){

		if ( GUILayout.Button( "Main Menu", _GUIStyles.navButton ) ){
			_GUIState = GUIStates.mainMenu;
			if ( Sounds.audioSource.isPlaying ) Sounds.audioSource.Stop();
		}
		
		scrollPosition = GUILayout.BeginScrollView( scrollPosition );
		{
			GUILayout.Label( "Tap different sections of the screen to control the ninja", _GUIStyles.textLabel );

			GUILayout.Box ( _GUIImages.instructions );
		}
		GUILayout.EndScrollView();
			
	}

	private GUIContent guiContent = new GUIContent();
	private string highlightedClip;

	void changeDirectory( string path ){
		currentPath = newPath = path;
		highlightedClip = null;
	}

	void loadNewClip( string filePath, bool setAsSelected = false ){
		string fileName = Path.GetFileName( filePath );

		if ( fileName != null ){
			bool clipIsInList = Sounds.loadedClips.Exists( a => a.filePath.Equals( filePath ) );
			
			if( !clipIsInList ){
				StartCoroutine( 
				    Sounds.loadFile( filePath, audioClip => {
						LoadedClip loadedClip = new LoadedClip( audioClip, fileName, filePath );
						Sounds.loadedClips.Add( loadedClip );
						if( setAsSelected ) Sounds.selectedClip = loadedClip;
						loadingSelected = false;
					})
				);
			}

		}
	}

	void soundSelect(){

		if ( GUILayout.Button( "Main Menu", _GUIStyles.navButton ) ){
			_GUIState = GUIStates.mainMenu;
			Sounds.audioSource.Stop();
		}

		GUILayout.BeginHorizontal();
		{
			// DISPLAY BACK BUTTON FOR NAVIGATION
			if( GUILayout.Button( _GUIImages.back, _GUIStyles.backButton ) ){
				DirectoryInfo parent = Directory.GetParent( currentPath );
				if( parent != null ){
					changeDirectory( parent.ToString() );

				}
			}
			
			newPath = GUILayout.TextField( newPath, _GUIStyles.cwdField );
			if( Directory.Exists( newPath ) && newPath != currentPath ){
				changeDirectory( newPath );
			}
		}
		GUILayout.EndHorizontal();
		
		List<string> audioFiles = new List<string>();
		
		// DISPLAY A LIST OF FOLDERS AND FILES
		scrollPosition = GUILayout.BeginScrollView( scrollPosition );
		{
			
			List<string> allowedExtensions = new List<string>{ 
				".mp3", ".ogg", ".wav", ".aiff", ".aif", ".mod", ".mod", ".it", ".s3m", ".xm" 
			};
			// GET THE FILE PATHS
			foreach( string filePath in Directory.GetFiles( currentPath )) {

				string extension = Path.GetExtension( filePath );

				// FILTER OUT ALL NON-AUDIO
				if( allowedExtensions.Contains ( extension ) ) audioFiles.Add( filePath );
				
			}
			
			if( audioFiles.Count > 0 ){
				
				GUILayout.Label( "Audio files", _GUIStyles.titleLabel );
				
				for( int i = 0; i < audioFiles.Count; i++ ){

					string filePath = audioFiles[ i ];
					string fileName = Path.GetFileName( filePath );

					guiContent.image = _GUIImages.music;
					guiContent.text = fileName;
					
					if( GUILayout.Button( guiContent, _GUIStyles.fileButton ) ){
						highlightedClip = filePath;
						Sounds.audioSource.Stop();

						loadNewClip( filePath );
					}
				}
			}
			
			// GET THE DIRECTORY PATHS
			string[] directoryPaths = GetDirectories( currentPath );

			if( directoryPaths != null ){
				if( directoryPaths.Length > 0 ){
					
					GUILayout.Label( "Directories", _GUIStyles.titleLabel );
					
					foreach( string directoryPath in directoryPaths ) {
						
						if ( Directory.Exists( directoryPath ) && GetDirectories( directoryPath ) != null ){
							
							string directoryName = Path.GetFileName( directoryPath );
							
							guiContent.image = _GUIImages.folder;
							guiContent.text = directoryName;
							
							if( GUILayout.Button( guiContent, _GUIStyles.dirButton ) ){
								changeDirectory( directoryPath );
							}
						}
					}
				}
			}

			
			if( audioFiles.Count < 1 ){
				GUILayout.Label( "There are no supported audio files in this directory", _GUIStyles.textLabel );
			}
		}
		GUILayout.EndScrollView();

		// PLAY/PAUSE AND ADD/REMOVE BUTTONS FOR SELECTED AUDIOFILE
		if( highlightedClip != null ){
			GUILayout.BeginHorizontal();
			{
				LoadedClip loadedClip = Sounds.loadedClips.Find( a => a.filePath.Equals( highlightedClip ) );

				if( loadedClip != null ){
					if ( GUILayout.Button( _GUIImages.reset, _GUIStyles.backButton ) ){
						Sounds.audioSource.time = 0;
					}
					
					if ( !Sounds.audioSource.isPlaying ){
						if( GUILayout.Button( _GUIImages.play, _GUIStyles.backButton ) ){
							Sounds.audioSource.clip = loadedClip.clip;
							Sounds.audioSource.Play();
						}
					} else {
						if( GUILayout.Button( _GUIImages.pause, _GUIStyles.backButton ) ){
							Sounds.audioSource.Pause();
						}
					}


					GUILayout.Label( loadedClip.fileName, _GUIStyles.textLabel );

					if( loadedClip != Sounds.selectedClip ){
						if( GUILayout.Button( _GUIImages.check, _GUIStyles.backButton ) ){
							Sounds.selectedClip = loadedClip;
							PlayerPrefs.SetString( "selectedClipPath", loadedClip.filePath );
						}
					} else {
						if( GUILayout.Button( _GUIImages.remove, _GUIStyles.backButton ) ){
							Sounds.selectedClip = null;
							highlightedClip = null;
						}
					}
				} else {
					GUILayout.Label( "Loading sound track...", _GUIStyles.textLabel );
				}

			}
			GUILayout.EndHorizontal();
		}
	}

	public string[] GetDirectories( string path ){
		try {
			return Directory.GetDirectories( path );
		} catch ( UnauthorizedAccessException ){
			return null;
		}
	}

}

[System.Serializable]
public class GUIStyles {
	public GUIStyle dirButton, backButton, fileButton, cwdField, titleLabel, fileLabel, navButton, textLabel;
}

[System.Serializable]
public class GUIImages {
	public Texture2D back, folder, music, play, pause, reset, check, remove, instructions, mainbg;
}













