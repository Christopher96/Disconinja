﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameUpdate : MonoBehaviour {

	public AudioClip defaultTrack;
	public static bool isPlaying = false;

	public static float currentScore = 0;
	public static float highScore = 0;

	public Text scoreText;
	public GameObject BGImage;
	public GameObject frequencyLine;
	
	void Start(){
		Sounds.audioSource = GetComponent<AudioSource>();
		Sounds.loadedClips = new List<LoadedClip>();
		Sounds.defaultTrack = defaultTrack;

		currentScore = 0;

		highScore = PlayerPrefs.GetFloat( "highScore" );
	}

	// Update is called once per frame
	void Update () {

		if ( currentScore > highScore ){
			highScore = currentScore;
		}

		PlayerPrefs.SetFloat( "highScore", highScore );

		BGImage.SetActive( !GameUpdate.isPlaying );
		scoreText.enabled = GameUpdate.isPlaying;
		frequencyLine.SetActive( GameUpdate.isPlaying );

		if ( isPlaying ){
			Time.timeScale = 1;

			if ( !Sounds.audioSource.isPlaying ){
				if ( Sounds.selectedClip != null ){
					Sounds.audioSource.clip = Sounds.selectedClip.clip;
				} else {
					Sounds.audioSource.clip = Sounds.defaultTrack;
				}
				Sounds.audioSource.Play();
			}

			scoreText.text = Mathf.Floor( GameUpdate.currentScore ).ToString() + "m";

		} else {
			Time.timeScale = 0;

			if ( !Sounds.audioSource.isPlaying ){
				Sounds.audioSource.Stop();
			}
		}

		if ( !Player.isDead ) colorLoop();
		else changeColor( Color.black );
	}

	public static void ReloadGame(){
		Player.isDead = false;
		Application.LoadLevel( Application.loadedLevel );
	}

	public Material mainMaterial;
	public Material lineMaterial;
	
	private int state = 0;
	private float step = 0.02f;

	void colorLoop(){
		Color color = mainMaterial.GetColor("_Color");
		
		switch ( state ){
		case 0: 
			if( color.b < 1 ) color.b+= step;
			else state++;
			break;
			
		case 1:
			if( color.r > 0.1 ) color.r-= step;
			else state++;
			break;
			
		case 2:
			if( color.g < 1 ) color.g+= step;
			else state++;
			break;
			
		case 3:
			if( color.b > 0.1 ) color.b-= step;
			else state++;
			break;
			
		case 4:
			if( color.r < 1 ) color.r+= step;
			else state++;
			break;
			
		case 5:
			if( color.g > 0.1 ) color.g-= step;
			else state = 0;
			break;
			
		}

		color.a = 1;
		
		changeColor( color );
	}

	void changeColor( Color color ){
		mainMaterial.SetColor( "_Color", color);
		lineMaterial.SetColor( "_TintColor", color );
	}
}

public class Sounds {
	public static AudioClip defaultTrack;
	public static LoadedClip selectedClip;
	public static AudioSource audioSource;
	public static List<LoadedClip> loadedClips;

	public static IEnumerator loadFile( string path, System.Action<AudioClip> result ) {
		WWW www = new WWW( "file://"+path );
		
		AudioClip audioFile = www.audioClip;
		
		while ( audioFile.loadState != AudioDataLoadState.Loaded )
			yield return www;
		
		result( www.GetAudioClip(false) );
	}
}

public class LoadedClip {
	public bool isSelected;
	public string fileName;
	public string filePath;
	public AudioClip clip;

	public LoadedClip( AudioClip clip, string fileName, string filePath, bool isSelected = false ){
		this.isSelected = isSelected;
		this.fileName = fileName;
		this.filePath = filePath;
		this.clip = clip;
	}
}













