﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnviromentUpdate : MonoBehaviour {

	public List<GameObject> obstaclePrefabs;
	public GameObject groundPrefab;
	public Transform spawner;

	// Use this for initialization
	void Start () {

		for( int i=0; i < 5; i++ ){
			if ( i>0 ) spawnObstacle();
			spawnGround();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void spawnGround(){
		Vector3 groundPos = groundPrefab.transform.position;
		Vector3 spawnerPos = spawner.position;
		Vector3 newPos = new Vector3( spawnerPos.x, groundPos.y, groundPos.z );
		GameObject ground = Instantiate( groundPrefab, newPos, Quaternion.identity ) as GameObject;
		spawnerPos.x += ground.GetComponent<BoxCollider2D>().bounds.size.x;
		spawner.position = spawnerPos;
	}

	void spawnObstacle(){
		GameObject obstacle = obstaclePrefabs[ Random.Range( 0, obstaclePrefabs.Count ) ];

		Vector3 obstaclePos = obstacle.transform.position;
		obstaclePos.x = spawner.position.x;
		Instantiate( obstacle, obstaclePos, Quaternion.identity );
	}

	void OnTriggerExit2D( Collider2D col ){
		//print( col.name );
	}
	
	void OnCollisionExit2D( Collision2D col ){
		//print( col.gameObject.name );
	}
}
