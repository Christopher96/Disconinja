﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBehaviour : MonoBehaviour {

	public Ground ground;
	public Movement movement;
	public Shuriken shuriken;

	private Rigidbody2D rb;
	private Animator anim;

	private float yStart;
	private float yPosition;

	public GameObject frequencyLine;

	void Start(){
		anim = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();

		yStart = transform.position.y;

		Player.isDead = false;
		Player.isSlicing = false;
		Player.isSliding = false;
		Player.isGrounded = true;
	}

	bool isPlayingState( string name ){
		if ( anim.GetCurrentAnimatorStateInfo (0).IsName (name) ) {
			return true;
		}

		return false;
	}
	
	void Update(){
		if ( !GameUpdate.isPlaying ) return;

		Player.isGrounded = ground.check.IsTouchingLayers( ground.mask );



		if ( Input.GetButtonDown( "Jump") && !isPlayingState( "Jumping" ) ){
			anim.SetTrigger("Jump");
		} else if ( Input.GetButtonDown( "Slide") && !isPlayingState( "Sliding" ) ){
			anim.SetTrigger("Slide");
		} else if ( Input.GetButtonDown( "Slice") && !isPlayingState( "Slicing" ) ){
			anim.SetTrigger("Slice");
		} else if ( Input.GetButtonDown( "Throw") && !isPlayingState( "Throwing" ) ){
			anim.SetTrigger("Throw");


		}
	
		foreach( Touch touch in Input.touches ){
			if( !Player.isDead ){

				// TAP CHECK IN ALL CORNERS FOR ANIMATION TRIGGERS
				if( touch.phase == TouchPhase.Began ){
					
					bool xBorder = touch.position.x > Screen.width / 2;
					bool yBorder = touch.position.y > Screen.height / 2;
					
					if( xBorder ){
						if( yBorder ){
							// UPPER RIGHT
							anim.SetTrigger("Jump");
							
						} else {
							// LOWER RIGHT
							anim.SetTrigger("Slide");
							
						}
					} else {
						if( yBorder ){
							// UPPER LEFT
							anim.SetTrigger("Slice");
							
						} else if ( shuriken.count > 0 ) {
							// LOWER LEFT
							anim.SetTrigger("Throw");
							
						}
					}

				}
			} else {

			}
		}

		yPosition = transform.position.y - yStart;

		// UPDATE ANIMATOR VALUES
		anim.SetBool( "isDead", Player.isDead );
		anim.SetBool( "isGrounded", Player.isGrounded );
		anim.SetBool( "isSliding", !GetComponent<BoxCollider2D>().enabled );
		anim.SetFloat( "yVelocity", rb.velocity.y );
		anim.SetFloat( "yPosition", Mathf.Floor( yPosition * 10 ) / 10 );

		// ALWAYS UPDATE UI TEXT
		//shuriken.textField.text = "shurikens: " + shuriken.count.ToString();
	}

	void FixedUpdate(){
		if ( !Player.isDead ){
			Vector2 pos = transform.position;
			pos.x += movement.speedCurve.Evaluate (Time.timeSinceLevelLoad) * movement.speed;
			transform.position = pos;

			GameUpdate.currentScore += 0.1f;
		}
	}
	
	void OnCollisionEnter2D( Collision2D col ){
		print (col.gameObject.tag);
		if ( GameUpdate.isPlaying ){
			switch( col.gameObject.tag ){
				case "Enemy":
					if( !Player.isSlicing ){
						Die();
					}
				break;
				
				case "Obstacle":
					Die();
				break;
			}
		}
	}

	void Die(){
		Player.isDead = true;
		anim.SetTrigger( "Die" );
		StartCoroutine( killPitch() );
	}

	void PauseGame(){
		GameUpdate.isPlaying = false;
	}

	void shurikenSpawn(){
		Instantiate( shuriken.prefab, 
		             shuriken.transform.position, 
		             shuriken.transform.rotation );

		//shuriken.textField.GetComponent<Outline>().effectColor = Color.black;
	}

	void shurikenCount(){
		if( shuriken.count > 0 ){
			shuriken.count--;
			//shuriken.textField.GetComponent<Outline>().effectColor = Color.white;
		}
	}

	void toggleSlicing(){
		Player.isSlicing = !Player.isSlicing;
	}

	void freezeCamera(){
		Camera.main.transform.SetParent( transform.parent );
	}

	void playerJump(){
		rb.velocity = new Vector3(0, movement.jumpForce, 0);
	}

	IEnumerator killPitch(){
		while( Sounds.audioSource.pitch > 0 ){
			Sounds.audioSource.pitch -= 0.01f;
			yield return null;
		}
	}

}

public static class Player {
	public static bool isDead = false;
	public static bool isGrounded = true;
	public static bool isSlicing = false;
	public static bool isSliding = false;
}

[System.Serializable]
public class Ground {
	public LayerMask mask;
	public CircleCollider2D check;
}

[System.Serializable]
public class Movement {
	public float speed;
	public float jumpForce;
	public AnimationCurve speedCurve;
}

[System.Serializable]
public class Shuriken {
	//public Text textField;
	public float count = 5;
	public GameObject prefab;
	public Transform transform;
}

















