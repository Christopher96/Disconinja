﻿using UnityEngine;
using System.Collections;

public class ShurikenBehaviour : MonoBehaviour {

	public float lifeTime;
	public float flyingSpeed;
	public float rotationSpeed;

	private float startTime;
	private bool hasHit = false;

	void Start(){
		startTime = Time.time;
	}

	void Update(){
		if ( Time.time - startTime > lifeTime ) Destroy( this.gameObject );
	}

	void OnTriggerEnter2D( Collider2D col ){
		if( !col.name.Contains( "Enemy") ){
			hasHit = true;
			GetComponent<PolygonCollider2D>().isTrigger = false;
			GetComponent<Rigidbody2D>().isKinematic = false;	
			lifeTime += 3;
		} else {
			Destroy( this.gameObject );
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if ( !hasHit ){
			Vector2 pos = transform.position;
			pos.x += flyingSpeed;
			transform.position = pos;
			
			Vector3 rot = transform.eulerAngles;
			rot.z -= rotationSpeed;
			transform.eulerAngles = rot;
		}
	}
}
