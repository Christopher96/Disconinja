﻿using UnityEngine;
using System.Collections;

public class CameraUpdate : MonoBehaviour {

	private float step = 0;

	// Update is called once per frame
	void Update () {

		Vector3 rotation = transform.eulerAngles;
		rotation.z = Mathf.Sin( step += 0.001f );
		transform.eulerAngles = rotation;
	}
}
