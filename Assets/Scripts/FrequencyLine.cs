﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FrequencyLine : MonoBehaviour {

	public int channel;
	public int numSamples;
	public int frequency;

	private Vector3 startPos;
	private Rigidbody2D rb;
	private Renderer rend;

	private Color mainColor;
	private Color secondColor;

	//public GameObject musicLine;
	public Text Scoretext;

	private int startFontSize;


	void Start(){
		rend = GetComponent<Renderer> ();
		rb = GetComponent<Rigidbody2D>();
		startPos = transform.position;

		secondColor = Color.white;
		mainColor = rend.material.GetColor("_TintColor");

		startFontSize = Scoretext.fontSize;
	}

	float prevNumb;

	// Update is called once per frame
	void Update () {
		if( GameUpdate.isPlaying ){
			float[] numbArray = new float[ 256 ];
			numbArray = Sounds.audioSource.GetSpectrumData( numSamples, channel, FFTWindow.BlackmanHarris );
			float number = numbArray[frequency];

			if( number*10 > 3 ){
				Scoretext.color = secondColor;
				StartCoroutine( increaseFontsize() );
				rend.material.SetColor("_TintColor", secondColor );
			} else {
				Scoretext.color = mainColor;
				StartCoroutine( decreaseFontsize() );
				rend.material.SetColor("_TintColor", mainColor );
			}

			Vector3 pos = transform.position;
			number = Mathf.Clamp( number*2 , 0, 3 );
			Vector3 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
			pos.y = startPos.y + playerPos.y + number;
			pos.x = startPos.x + playerPos.x;
			rb.MovePosition( pos );     

			/*
			EllipsoidParticleEmitter emitter = musicLine.GetComponent<EllipsoidParticleEmitter>();
			float rate = number*20;
			emitter.maxEmission = rate;
			emitter.minEmission = rate;
			*/

		}
	}

	int maxSize = 5;

	IEnumerator increaseFontsize(){
		while( Scoretext.fontSize < startFontSize + maxSize ){
			Scoretext.fontSize += 1;
			yield return null;
		}
	}

	IEnumerator decreaseFontsize(){
		while( Scoretext.fontSize > startFontSize ){
			Scoretext.fontSize -= 1;
			yield return null;
		}
	}
}